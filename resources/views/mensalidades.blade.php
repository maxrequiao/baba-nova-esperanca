<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Situação de Pagamentos</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="col-lg-12">
        <h1>Situação de Pagamentos</h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Jogador</th>
                    <th>Pagamento</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $date1 = '2022-01-22';
                    $date2 = date('Y-m-d');
                    $d1 = new DateTime($date2);
                    $d2 = new DateTime($date1);
                    $Months = $d2->diff($d1);
                    $howeverManyMonths = (($Months->y) * 12) + ($Months->m);
                ?>


                @foreach($users as $user)
                    @if(($howeverManyMonths - $user->total) >= 1)
                        <tr class="table-danger">
                            <td>{{$user->associados->nome}}</td>
                            <td>nao está apto</td>
                        </tr>
                    @else
                        <tr class="table-success">
                            <td>{{$user->associados->nome}}</td>
                            <td>está apto</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" ></script>
</body>
</html>
