@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <div class="card rounded-0 text-light bg-info">
                <div class="card-body">
                    <h3 class="card-title">Associado: {{$associado->nome}}</h3>
                    <p class="card-text mb-0"><b>Nome:</b> {{$associado->nome}}</p>
                    <p class="card-text mb-0"><b>Apelido:</b> {{$associado->apelido}}</p>
                    <p class="card-text mb-0"><b>Contato:</b> {{$associado->contato}}</p>
                    <p class="card-text mb-3"><b>Nascimemnto:</b> @isset($associado->nascimento){{$associado->nascimento->format('Y-m-d')}}@endisset</p>
                    {{--                            <div class="mb-3">--}}
                    {{--                                <label for="time">Time (<span class="small text-muted">ou apelido</span>)</label>--}}
                    {{--                                <select name="time" id="time" class="form-control">--}}
                    {{--                                    <option value="">Selecione</option>--}}
                    {{--                                    <option value=""></option>--}}
                    {{--                                </select>--}}
                    {{--                            </div>--}}
                </div>
            </div>
        </div>
        <div class="col-lg-10 pt-3">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">
                        Pagamentos
                        <a href="{{route('associados.financeiros.index',$associado->id)}}" class="btn btn-primary btn-sm float-end">+add <i class="fa fa-dollar"></i></a>
                    </h3>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Descrição</th>
                                <th>Valor</th>
                                <th>Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($financeiros as $f)
                                <tr class="{{$f->tipo == 'e'?'text-success':'text-danger'}}">
                                    <td>
                                        @if($f->tipo == 'e')
                                            {{$f->mensalidade == '1'?'Mensalidade':$f->description}}: {{$f->associados->nome}}
                                        @else
                                            {{$f->description}}
                                        @endif
                                    </td>
                                    <td><b>{{ number_format($f->valor,2,',','.')}}</b></td>
                                    <td>{{$f->data->format('d/m/Y')}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">Sem Movimentações</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
