@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>Mensalidades de {{$associado->nome}}</h3>
        </div>
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('associados.financeiros.store',$associado->id)}}" method="post" class="row g-3">
                        @csrf
                        <input type="hidden" name="tipo" value="e">
                        <input type="hidden" name="associado_id" value="{{$associado->id}}">
                        <div class="col-md-4 mb-1">
                            <label for="valor">Valor</label>
                            <input type="text" name="valor" class="form-control rounded-0 valor" required autocomplete="off"/>
                        </div>
                        <div class="col-md-4 mb-1">
                            <label for="data">Data</label>
                            <select name="data" id="data" class="form-select" required>
                                <option value="">Selecione</option>
                                <option value="2022-01-22" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-01-22','mensalidade'=>'1'])->first()) disabled @endif>22 Jan</option>
                                <option value="2022-02-19" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-02-19','mensalidade'=>'1'])->first()) disabled @endif>19 Fev</option>
                                <option value="2022-03-19" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-03-19','mensalidade'=>'1'])->first()) disabled @endif>19 Mar</option>
                                <option value="2022-04-23" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-04-23','mensalidade'=>'1'])->first()) disabled @endif>23 Abr</option>
                                <option value="2022-05-21" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-05-21','mensalidade'=>'1'])->first()) disabled @endif>21 Mai</option>
                                <option value="2022-06-18" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-06-18','mensalidade'=>'1'])->first()) disabled @endif>18 Jun</option>
                                <option value="2022-07-23" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-07-23','mensalidade'=>'1'])->first()) disabled @endif>23 Jul</option>
                                <option value="2022-08-20" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-08-20','mensalidade'=>'1'])->first()) disabled @endif>20 Ago</option>
                                <option value="2022-09-17" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-09-17','mensalidade'=>'1'])->first()) disabled @endif>17 Set</option>
                                <option value="2022-10-22" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-10-22','mensalidade'=>'1'])->first()) disabled @endif>22 Out</option>
                                <option value="2022-11-19" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-11-19','mensalidade'=>'1'])->first()) disabled @endif>19 Nov</option>
                                <option value="2022-12-24" @if(\App\Models\Financeiro::select('data')->where(['associado_id'=>$associado->id,'data'=>'2022-12-24','mensalidade'=>'1'])->first()) disabled @endif>24 Dez</option>
                            </select>
                        </div>
                        <div class="col-md-4 mb-1">
                            <label for="nome" class="d-block mb-2">Mensalidade</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="mensalidade" id="mensalidade1" value="1" required>
                                <label class="form-check-label" for="mensalidade1">Sim</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="mensalidade" id="mensalidade2" value="0" required>
                                <label class="form-check-label" for="mensalidade2">Não</label>
                            </div>
                        </div>
                        <div class="mb-1">
                            <label for="description">Descrição</label>
                            <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                        <div id="s" class="d-none">
                            <div class="mb-3">
                                <label for="description1">Descrição</label>
                                <textarea name="description1" id="description1" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="">
                            <button type="submit" class="btn btn-primary btn-sm rounded-0 float-end">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h3>Pagamentos</h3>
                    @foreach($financeiros as $f)
                        <li>{{$f->data->format('d M Y')}} (@if($f->mensalidade == '1') mensalidade @else outros @endif )</li>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="/js/jquery.mask.js"></script>
    <script>
        $(document).ready(function(){
            $('.valor').mask('999.999,99',{reverse: true});
        });
        window.onload = function(){
            onclick = function(){
                var tipo = document.querySelector('input[name = tipo]:checked').value;
                if(tipo == 'e'){
                    $('#e').removeClass('d-none');
                    $('#s').addClass('d-none');
                }else if(tipo == 's'){
                    $('#s').removeClass('d-none');
                    $('#e').addClass('d-none');
                }
            }
        }
    </script>
@endpush
