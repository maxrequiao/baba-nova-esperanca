@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>Adicionar Associado</h3>
        </div>
        <div class="col-md-8">
            <div class="card rounded-0">
                <div class="card-body">
                    <form action="{{route('associados.store')}}" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="nome">Nome (<span class="small text-muted">ou apelido</span>)</label>
                            <input type="text" name="nome" class="form-control rounded-0" required>
                        </div>
                        <div class="mb-3">
                            <label for="apelido">Apelido (<span class="small text-muted">opcional</span>)</label>
                            <input type="text" name="apelido" class="form-control rounded-0">
                        </div>
                        <div class="mb-3">
                            <label for="contato">Contato (<span class="small text-muted">opcional</span>)</label>
                            <input type="tel" name="contato" maxlength="11" class="form-control rounded-0">
                        </div>
                        <div class="mb-3">
                            <label for="nascimento">Nascimento (<span class="small text-muted">opcional, só números</span>)</label>
                            <input type="date" name="nascimento" class="form-control rounded-0">
                        </div>
                        <div class="">
                            <button type="submit" class="btn btn-primary btn-sm rounded-0 float-end">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
