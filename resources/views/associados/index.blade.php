@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg">
            <h3 class="fw-light">
                Associados
                <a class="btn btn-sm btn-primary float-end" href="{{route('associados.create')}}">+add Associado</a>
            </h3>
            <form action="{{route('associados.search')}}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" name="q" class="form-control" placeholder="Nome ou Apelido" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Nome/Apelido</th>
                            <th>Contato</th>
                            <th>Nascimento</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($associados as $a)
                            <tr>
                                <td>{{$a->id}}</td>
                                <td>{{$a->nome}}</td>
                                <td>{{$a->contato}}</td>
                                <td>{{ $a->nascimento ?$a->nascimento->format('d/m/Y'):''}}</td>
                                <td><a class="btn btn-info btn-sm rounded-0" href="{{route('associados.edit',$a->id)}}"><i class="fa fa-edit"></i></a></td>
                                <td><a class="btn btn-primary btn-sm rounded-0" href="{{route('associados.show',$a->id)}}"><i class="fa fa-eye"></i></a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Sem Associados</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            {{$associados->links()}}
        </div>
    </div>
@endsection
