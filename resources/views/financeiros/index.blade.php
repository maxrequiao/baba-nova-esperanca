@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="fw-lighter">
                Finanças
                <a class="btn btn-sm btn-primary float-end" href="{{route('financeiros.create')}}">+add</a>
            </h3>

        </div>
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($financeiros as $f)
                        <tr class="{{$f->tipo == 'e'?'text-success':'text-danger'}}">
                            <td>
                            @if($f->tipo == 'e')
                                {{$f->mensalidade == '1'?'Mensalidade':$f->description}}: {{$f->associados->nome}}
                            @else
                                {{$f->description}}
                            @endif
                            </td>
                            <td><b>{{ number_format($f->valor,2,',','.')}}</b></td>
                            <td>{{$f->data->format('d/m/Y')}}</td>
                            <td><a class="btn btn-info btn-sm rounded-0" href="{{route('financeiros.edit',$f->id)}}"><i class="fa fa-edit"></i></a></td>
                            <td>
                                <form action="{{route('financeiros.destroy',$f->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm rounded-0" href=""><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">Sem Movimentações</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            {{$financeiros->links()}}
        </div>
    </div>
@endsection
