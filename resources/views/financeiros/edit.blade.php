@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
    <div class="row">
        <div class="col-lg-12">
            <h3>Adicionar</h3>
        </div>
        <div class="col-md-8">
            <div class="card rounded-0">
                <div class="card-body">
                    <form action="{{route('financeiros.update',$financeiro->id)}}" method="post" class="row g-3">
                        @csrf
                        @method('PUT')
                        <div class="col-md-2 mb-3 me-3">
                            <label for="valor">Valor</label>
                            <input type="text" name="valor" class="form-control rounded-0 valor" value="{{number_format($financeiro->valor,2,',','.')}}" required>
                        </div>
                        <div class="col-md-5 mb-3">
                            <label for="nome" class="d-block mb-2">Tipo</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="tipo" id="inlineRadio1" value="e" {{$financeiro->tipo == 'e'?'checked':''}} required>
                                <label class="form-check-label" for="inlineRadio1">Entrada</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="tipo" id="inlineRadio2" value="s" {{$financeiro->tipo == 's'?'checked':''}} required>
                                <label class="form-check-label" for="inlineRadio2">Saida</label>
                            </div>
                        </div>
                        <div id="e" class="d-none">
                            <div class="col-md-4 mb-3">
                                <label for="nome" class="d-block mb-2">Mensalidade</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="mensalidade" id="mensalidade1" value="1" {{$financeiro->mensalidade == '1'?'checked':''}} required>
                                    <label class="form-check-label" for="mensalidade1">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="mensalidade" id="mensalidade2" value="0" {{$financeiro->mensalidade == '0'?'checked':''}} required>
                                    <label class="form-check-label" for="mensalidade2">Não</label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="associado">Associado</label>
                                <select name="associado_id" id="associado_id" class="form-control js-example-basic-single">
                                    <option value="">Selecione</option>
                                    @foreach($associados as $a)
                                        <option value="{{$a->id}}" @if($financeiro->associado_id == $a->id) selected @endif>{{$a->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="description">Descrição</label>
                                <textarea name="description" id="description" class="form-control" cols="30" rows="10">{{$financeiro->description}}</textarea>
                            </div>
                        </div>
                        <div id="s" class="d-none">
                            <div class="mb-3">
                                <label for="description1">Descrição</label>
                                <textarea name="description1" id="description1" class="form-control" cols="30" rows="10">{{$financeiro->description}}</textarea>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="data">Data</label>
                            <input type="date" name="data" class="form-control rounded-0" value="{{$financeiro->data->format('Y-m-d')}}" required>
                        </div>
                        <div class="">
                            <button type="submit" class="btn btn-primary btn-sm rounded-0 float-end">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="/js/jquery.mask.js"></script>
    <script>
        $(document).ready(function(){
            $('.valor').mask('999.999,99',{reverse: true});
        });
        window.onload = function(){
            onclick = function(){
                var tipo = document.querySelector('input[name = tipo]:checked').value;
                if(tipo == 'e'){
                    $('#e').removeClass('d-none');
                    $('#s').addClass('d-none');
                }else if(tipo == 's'){
                    $('#s').removeClass('d-none');
                    $('#e').addClass('d-none');
                }
            }
        }
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                theme: 'bootstrap4',
            });
            @if($financeiro->tipo == 'e')
                $('#inlineRadio1').attr('checked', true);
                $('#e').removeClass('d-none');
                $('#s').addClass('d-none');
            @else
                $('#inlineRadio2').attr('checked', true);
                $('#s').removeClass('d-none');
                $('#e').addClass('d-none');
            @endif
        });
    </script>
@endpush
