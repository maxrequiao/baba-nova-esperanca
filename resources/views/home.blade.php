@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-3 mb-3">
            <div class="card border-success text-success shadow">
                <div class="card-body">
                    <p class="mb-0 mx-auto">Mensalidades</p>
                    <span class="fs-3 float-start">R$ {{number_format($mensalidadeSum,2,',','.')}}</span>
                    <span class="fs-3 float-end"><i class="fa fa-file-invoice-dollar"></i></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card border-success text-success shadow">
                <div class="card-body">
                    <p class="mb-0 mx-auto">Outras Entradas</p>
                    <span class="fs-3 float-start">R$ {{number_format($outroSum,2,',','.')}}</span>
                    <span class="fs-3 float-end"><i class="fa fa-dollar"></i></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card border-danger text-danger shadow">
                <div class="card-body">
                    <p class="mb-0 mx-auto">Pagamentos/Saídas</p>
                    <span class="fs-3 float-start">R$ {{number_format($pagamentoSum,2,',','.')}}</span>
                    <span class="fs-3 float-end"><i class="fa fa-funnel-dollar"></i></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card border-info text-info shadow">
                <div class="card-body">
                    <p class="mb-0 mx-auto">Total em Caixa</p>
                    <?php $total = ($mensalidadeSum + $outroSum) - $pagamentoSum?>
                    <span class="fs-3 float-start">R$ {{number_format($total,2,',','.')}}</span>
                    <span class="fs-3 float-end"><i class="fa fa-comments-dollar"></i></span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                    <p class="mb-0 mx-auto">Associados</p>
                    <span class="fs-1 float-start ">{{$associadosCount}}</span>
                    <span class="fs-1 float-end "><i class="fa fa-users"></i></span>
                </div>
            </div>
        </div>
    </div>
@endsection
