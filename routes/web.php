<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/mensalidades', function (){
    $users = \App\Models\Financeiro::where('mensalidade','1')->groupBy('associado_id')
                                    ->select('associado_id','mensalidade', DB::raw('count(*) as total'))
                                    ->orderBy('total','desc')
                                    ->get();
    return view('mensalidades',compact('users'));
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/associados','\App\Http\Controllers\AssociadoController');
Route::get('/associados/{id}/financeiro',[\App\Http\Controllers\AssociadoController::class,'financeiro'])->name('associados.financeiros.index');
Route::any('/associados/search',[\App\Http\Controllers\AssociadoController::class,'search'])->name('associados.search');
Route::post('/associados/{id}/financeiro',[\App\Http\Controllers\AssociadoController::class,'financeiroStore'])->name('associados.financeiros.store');
Route::resource('/financeiros','\App\Http\Controllers\FinanceiroController');
