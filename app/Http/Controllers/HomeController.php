<?php

namespace App\Http\Controllers;

use App\Models\Associado;
use App\Models\Financeiro;
use App\Models\User;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $associadosCount  = Associado::count();
        $mensalidadeSum = Financeiro::where('mensalidade','1')->sum('valor');
        $outroSum = Financeiro::where('mensalidade','0')->sum('valor');
        $pagamentoSum = Financeiro::where('tipo','s')->sum('valor');
        $associados = Associado::select('id')->get();
//        foreach ($associados as $a) {
//            $mes = Financeiro::select()
//        }

        return view('home',compact('associadosCount','mensalidadeSum','pagamentoSum','outroSum'));
    }
}
