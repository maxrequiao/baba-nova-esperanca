<?php

namespace App\Http\Controllers;

use App\Models\Associado;
use App\Models\Financeiro;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class FinanceiroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $financeiros = Financeiro::orderBy('id','desc')->paginate(15);
        return view('financeiros.index', compact('financeiros'));
    }

    public function create()
    {
        $associados = Associado::select('id','nome')->get();
        return view('financeiros.create',compact('associados'));
    }

    public function store(Request $request)
    {
        $fin = new Financeiro();
        $fin->valor = str_replace(array(".", ","), array("", "."), $request->valor);
        if($request->tipo == 'e'){
            $fin->associado_id = $request->associado_id;
            $fin->mensalidade = $request->mensalidade;
            $fin->description = $request->description;
        } else if ($request->tipo == 's'){
            $fin->description = $request->description1;
        }
        $fin->tipo = $request->tipo;
        $fin->data = $request->data;
        $fin->save();

        return redirect()->route('financeiros.index')->with('success','Inserido com Sucesso!');
    }

    public function edit(Financeiro $financeiro)
    {
        $associados = Associado::select('id','nome')->get();
        return view('financeiros.edit',compact('financeiro','associados'));
    }

    public function update(Request $request, Financeiro $financeiro)
    {

        $financeiro->valor = str_replace(array(".", ","), array("", "."), $request->valor);
        if($request->tipo == 'e'){
            $financeiro->associado_id = $request->associado_id;
            $financeiro->mensalidade = $request->mensalidade;
            $financeiro->description = $request->description;
        } else if ($request->tipo == 's'){
            $financeiro->description = $request->description1;
        }
        $financeiro->tipo = $request->tipo;
        $financeiro->data = $request->data;
        $financeiro->save();

        return redirect()->route('financeiros.edit',$financeiro->id)->with('success','Atualizado com Sucesso!');
    }

    public function destroy(Financeiro $financeiro)
    {
        $financeiro->delete();
        return redirect()->route('financeiros.index')->with('success','Deletado com Sucesso!');
    }
}
