<?php

namespace App\Http\Controllers;

use App\Models\Associado;
use App\Models\Financeiro;
use Illuminate\Http\Request;

class AssociadoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $associados = Associado::orderBy('id','desc')->paginate(10);
        return view('associados.index',compact('associados'));
    }

    public function create()
    {
        return view('associados.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(),[
            'nome' => ['required', 'string', 'max:255'],
        ]);

        $associado = new Associado();
        $associado->nome = $request->nome;
        $associado->apelido = $request->apelido;
        $associado->contato = $request->contato;
        $associado->nascimento = $request->nascimento;
        $associado->save();

        return redirect()->route('associados.index')->with('success','Associado Inserido com Sucesso');
    }

    public function show(Associado $associado)
    {
        $financeiros = Financeiro::where('associado_id',$associado->id)->orderBy('data','asc')->get();
        return view('associados.show',compact('associado','financeiros'));
    }

    public function financeiro($id)
    {
        $associado = Associado::find($id);
        $financeiros = Financeiro::where('associado_id',$id)->orderBy('data','asc')->get();
        return view('associados.financeiro',compact('associado','financeiros'));
    }

    public function financeiroStore(Request $request, $id)
    {
        $fin = new Financeiro();
        $fin->valor = str_replace(array(".", ","), array("", "."), $request->valor);
        if($request->tipo == 'e'){
            $fin->associado_id = $request->associado_id;
            $fin->mensalidade = $request->mensalidade;
            $fin->description = $request->description;
        } else if ($request->tipo == 's'){
            $fin->description = $request->description1;
        }
        $fin->tipo = $request->tipo;
        $fin->data = $request->data;
        $fin->save();

        return redirect()->route('associados.financeiros.index',$id)->with('success','Inserido com Sucesso!');
    }

    public function edit(Associado $associado)
    {
        return view('associados.edit',compact('associado'));
    }

    public function update(Request $request, Associado $associado)
    {
        $this->validate(request(),[
            'nome' => ['required', 'string', 'max:255'],
        ]);

        $associado->nome = $request->nome;
        $associado->apelido = $request->apelido;
        $associado->contato = $request->contato;
        $associado->nascimento = $request->nascimento;
        $associado->save();

        return redirect()->route('associados.edit',$associado)->with('success','Associado Atualizado com Sucesso');
    }

    public function search()
    {
        $q = \Request::get('q');
        $associados = Associado::where('nome','like','%'.$q.'%')
                              ->orWhere('apelido','like','%'.$q.'%')
                              ->get();
        if($associados->count() > 0){
            return view('associados.search',compact('associados'));
        }else{
            return back()->with('warning','Associado Não Encontrado');
        }
    }

    public function destroy(Associado $associado)
    {
        //
    }
}
