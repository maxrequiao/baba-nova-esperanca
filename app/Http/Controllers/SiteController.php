<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        return view('site.index');
    }

    public function jogos()
    {
        return view('site.jogos');
    }

    public function gols()
    {
        return view('site.jogos');
    }
}
