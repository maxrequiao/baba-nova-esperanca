<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Financeiro extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['data','deleted_at'];

    public function associados()
    {
        return $this->belongsTo(Associado::class,'associado_id');
    }
}
